package com.ynov.demo.controller;

import com.ynov.demo.domain.Appartement;
import com.ynov.demo.domain.AppartementDto;
import com.ynov.demo.service.AppartementService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/rest")
public class AppartementController {
    AppartementService appartementService;

    public AppartementController(AppartementService appartementService) {
        this.appartementService = appartementService;
    }

    @GetMapping("/appartements")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartement> getAllAppartements() {
        return appartementService.getAllAppartements();
    }

    @PostMapping("/appartement/create/{nom}/{surface}/{nombre_couchage}/{equipement_bebe}/{climatisation}/{prix}/{residence_id}")
    @ResponseStatus(HttpStatus.OK)
    public Appartement createAppartement(@PathVariable String nom, @PathVariable int surface, @PathVariable int prix,
                                         @PathVariable int nombre_couchage, @PathVariable boolean equipement_bebe,
                                         @PathVariable boolean climatisation, @PathVariable Long residence_id) {
        Appartement application = appartementService.createApp(nom, surface, nombre_couchage, equipement_bebe, climatisation, residence_id, prix);
        return application;
    }

    @PutMapping("/appartement/update/{id}/{nom}/{surface}/{nombre_couchage}/{equipement_bebe}/{climatisation}/{prix}/{residence_id}")
    @ResponseStatus(HttpStatus.OK)
    public Appartement updateAppartement(@PathVariable Long id, @PathVariable String nom, @PathVariable int prix,
                                         @PathVariable int nombre_couchage, @PathVariable boolean equipement_bebe,@PathVariable int surface,
                                         @PathVariable boolean climatisation, @PathVariable Long residence_id) {
        Appartement application = appartementService.updateApp(id, nom, surface, nombre_couchage, equipement_bebe, climatisation, residence_id, prix);
        return application;
    }

    @GetMapping("/appartements/AppartementRegion/{region}")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartement>  AppartementRegion(@PathVariable String region) {
        return appartementService.AppartementRegion(region);
    }

    @GetMapping("/appartements/AppartementPiscine")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartement>  AppartementPiscine() {
        return appartementService.AppartementPiscine();
    }

    @GetMapping("/appartements/AppartementMontagne")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartement>  AppartementMontagne() {
        return appartementService.AppartementMontagne();
    }

    @GetMapping("/appartements/AppartementAvecId_Prix/{beginDate}/{endDate}")
    @ResponseStatus(HttpStatus.OK)
    public List<AppartementDto>  AppartementAvecId_Prix(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate beginDate,
                                                               @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return appartementService.AppartementAvecId_Prix(beginDate, endDate);
    }

    @GetMapping("/appartements/AppartementDto/{beginDate}/{endDate}/{region}")
    @ResponseStatus(HttpStatus.OK)
    public List<AppartementDto>  AppartementDto(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate beginDate,
                                                               @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
                                                   @PathVariable String region) {
        return appartementService.AppartementDto(beginDate, endDate, region);
    }

    @DeleteMapping("/appartement/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteAppartement(@PathVariable Long id) {
        appartementService.deleteApp(id);
    }

}

package com.ynov.demo.controller;

import com.ynov.demo.domain.Residence;
import com.ynov.demo.service.ResidenceService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class ResidenceController {
    ResidenceService residenceservice;

    public ResidenceController(ResidenceService Residenceservice) {
        this.residenceservice = Residenceservice;
    }

    @GetMapping("/residences")
    @ResponseStatus(HttpStatus.OK)
    public List<Residence> getResidence() { return residenceservice.getResidence(); }

    @PostMapping("/residence/create/{nom}/{type_residence}/{pays}/{region}/{adresse}/{gps}/{type_lieu}")
    @ResponseStatus(HttpStatus.OK)
    public Residence createResidence(@PathVariable String nom, @PathVariable String type_residence, @PathVariable String pays, @PathVariable String region, @PathVariable String adresse, @PathVariable String gps, @PathVariable String type_lieu) {
        return residenceservice.createResidence(nom, type_residence, pays, region, adresse, gps, type_lieu);
    }

    @PutMapping("/residence/update/{id}/{nom}/{type_residence}/{pays}/{region}/{adresse}/{gps}/{type_lieu}")
    @ResponseStatus(HttpStatus.OK)
    public Residence updateResidence(@PathVariable Long id, @PathVariable String nom, @PathVariable String type_residence, @PathVariable String pays, @PathVariable String region, @PathVariable String adresse, @PathVariable String gps, @PathVariable String type_lieu) {
        return residenceservice.updateResidence(id, nom, type_residence, pays, region, adresse, gps, type_lieu);
    }

    @GetMapping("/residence/getAllResidenceForAPays/{pays}")
    @ResponseStatus(HttpStatus.OK)
    public List<Residence> ResidencePays(@PathVariable String pays) {
        return residenceservice.ResidencePays(pays);
    }

    @DeleteMapping("/residence/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteResidence(@PathVariable Long id) {
        residenceservice.deleteResidence(id);
    }

}

package com.ynov.demo.controller;

import com.ynov.demo.domain.ServiceResidence;
import com.ynov.demo.service.ServiceResidenceService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class ServiceResidenceController {
    ServiceResidenceService ServiceResidenceService;

    public ServiceResidenceController(ServiceResidenceService ServiceResidenceService) {
        this.ServiceResidenceService = ServiceResidenceService;
    }

    @GetMapping("/services")
    @ResponseStatus(HttpStatus.OK)
    public List<ServiceResidence> getServices() {
        return ServiceResidenceService.getServices();
    }

    @PostMapping("/service/create/{detail_service}/{residence_id}")
    @ResponseStatus(HttpStatus.OK)
    public ServiceResidence createService(@PathVariable String detail_service, @PathVariable Long residence_id) {
        return ServiceResidenceService.createService(detail_service, residence_id);
    }

    @PutMapping("/service/update/{id}/{detail_service}/{residence_id}")
    @ResponseStatus(HttpStatus.OK)
    public ServiceResidence updateService(@PathVariable Long id, @PathVariable String detail_service, @PathVariable Long residence_id){
        return ServiceResidenceService.updateService(id, detail_service, residence_id);
    }

    @DeleteMapping("/service/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteServiceResidence(@PathVariable Long id) {
        ServiceResidenceService.deleteServiceResidence(id);
    }

}

package com.ynov.demo.repository;

import com.ynov.demo.domain.ServiceResidence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceResidenceRepository extends JpaRepository<ServiceResidence, Long> {
    @Query(value = "SELECT serviceResidence FROM ServiceResidence serviceResidence where serviceResidence.id = :#{#id} ")
    ServiceResidence findServiceById(@Param("id") Long id);

    @Query(value = "SELECT DISTINCT serviceResidence FROM ServiceResidence serviceResidence ")
    List<ServiceResidence> getAllServices();
}

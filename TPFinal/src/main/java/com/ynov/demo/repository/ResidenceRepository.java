package com.ynov.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ynov.demo.domain.Residence;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResidenceRepository extends JpaRepository<Residence, Long> {
    @Query(value = "SELECT residence FROM Residence residence where residence.pays = :#{#pays} ")
    List<Residence> ResidencePays(@Param("pays") String pays);

    @Query(value = "SELECT DISTINCT residence FROM Residence residence   ")
    List<Residence> getAllResidence();

    @Query(value = "SELECT residence FROM Residence residence where residence.id = :#{#id} ")
    Residence findResidenceById(@Param("id") Long id);

    @Query(value = "SELECT * FROM residence JOIN appartement ON  residence.id = appartement.complexe_id where appartement.id = :#{#id}", nativeQuery = true)
    Residence findResidenceIdWithAnAppId(@Param("id") Long id);
}

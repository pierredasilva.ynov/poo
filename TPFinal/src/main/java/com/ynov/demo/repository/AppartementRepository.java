package com.ynov.demo.repository;

import com.ynov.demo.domain.Appartement;
import com.ynov.demo.domain.AppartementDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface AppartementRepository extends JpaRepository<Appartement, Long> {
    @Query(value = "SELECT * FROM appartement JOIN residence ON appartement.residence_id = residence.id \n" +
            "WHERE residence.region = :#{#region}", nativeQuery = true)
    List<Appartement> AppartementRegion(@Param("region") String region);

    @Query(value = "SELECT Appartement FROM Appartement Appartement where Appartement.id = :#{#id} ")
    Appartement findAppId(@Param("id") Long id);

    @Query(value = "SELECT * FROM appartement JOIN residence ON appartement.residence_id = residence.id \n" +
            "WHERE appartement.residence_id = :#{#id}", nativeQuery = true)
    List<Appartement> findAppartementsWithResidenceId(@Param("id") Long id);

    @Query(value = "SELECT * FROM appartement JOIN residence ON appartement.residence_id = residence.id \n" +
            "JOIN my_service ON residence.id = my_service.residence_id \n" +
            "WHERE my_service.detail_service = 'piscine'", nativeQuery = true)
    List<Appartement> AppartementPiscine();

    @Query(value = "SELECT * FROM appartement JOIN residence ON appartement.residence_id = residence.id \n" +
            "WHERE residence.type_lieu = 'montagne'", nativeQuery = true)
    List<Appartement> AppartementMontagne();

    @Query(value = "SELECT NEW com.ynov.demo.domain.AppartementDto(appartement.id, residence.type_lieu, appartement.prix) " +
                    "from Residence residence " +
                    "join residence.appartements appartement" +
                    " join appartement.reservationDates date " +
                    "WHERE (:beginDate < date.beginDate and :endDate < date.beginDate ) " +
                    "AND LOWER(residence.type_lieu) = 'mer' " +
                    "OR ( :beginDate > date.endDate and :endDate > date.endDate) " +
                    "order by appartement.prix DESC")
    List<AppartementDto> AppartementAvecId_Prix(LocalDate beginDate, LocalDate endDate);

    @Query(value = "SELECT NEW com.ynov.demo.domain.AppartementDto(appartement.id, residence.type_lieu, appartement.prix, residence.nom) " +
            "from Residence residence " +
            "join residence.appartements appartement" +
            " join appartement.reservationDates date " +
            " join residence.serviceResidences serviceResidence " +
            "WHERE  ( LOWER(residence.type_lieu) = 'mer' and LOWER(serviceResidence.detailService) = 'piscine' and residence.region = :region and appartement.nombre_couchage >= 4 ) " +
            "AND ( (:beginDate < date.beginDate and :endDate < date.beginDate ) " +
            "OR ( :beginDate > date.endDate and :endDate > date.endDate) ) " +
            "order by appartement.prix ASC")
    List<AppartementDto> AppartementDto(LocalDate beginDate, LocalDate endDate, String region);

}

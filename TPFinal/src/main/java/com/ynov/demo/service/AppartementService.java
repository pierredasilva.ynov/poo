package com.ynov.demo.service;

import com.ynov.demo.domain.Appartement;
import com.ynov.demo.domain.AppartementDto;
import com.ynov.demo.domain.Residence;
import com.ynov.demo.repository.AppartementRepository;
import com.ynov.demo.repository.ResidenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class AppartementService {
    private final AppartementRepository appartementRepository;
    private ResidenceRepository residenceRepository;

    public AppartementService(AppartementRepository appartementRepository, ResidenceRepository residenceRepository) {
        this.appartementRepository = appartementRepository;
        this.residenceRepository = residenceRepository;
    }

    public List<Appartement> getAllAppartements() {
        return appartementRepository.findAll();
    }

    @Transactional
    public Appartement createApp(String nom, int surface, int nombre_couchage, boolean equipement_bebe,
                                 boolean climatisation, Long residence_id, int prix) {
        Residence residence = residenceRepository.findResidenceById(residence_id);
        Appartement app = new Appartement();
        app.setSurface(surface);
        app.setPrix(prix);
        app.setNom(nom);
        app.setNombre_couchage(nombre_couchage);
        app.setClimatisation(equipement_bebe);
        app.setEquipement_bebe(climatisation);
        appartementRepository.save(app);
        residence.getAppartements().add(app);
        return app;
    }

    @Transactional
    public Appartement updateApp(Long id, String nom, int surface, int nombre_couchage, boolean equipement_bebe,
                                 boolean climatisation, Long residence_id, int prix) {
        Residence oldResidence = residenceRepository.findResidenceIdWithAnAppId(id);
        Appartement app = appartementRepository.findAppId(id);
        oldResidence.getAppartements().remove(app);
        Residence residence = residenceRepository.findResidenceById(residence_id);
        app.setSurface(surface);
        app.setNom(nom);
        app.setPrix(prix);
        app.setNombre_couchage(nombre_couchage);
        app.setClimatisation(equipement_bebe);
        app.setEquipement_bebe(climatisation);
        appartementRepository.save(app);
        residence.getAppartements().add(app);
        return app;
    }

    public List<Appartement> AppartementRegion(String region) {
        return appartementRepository.AppartementRegion(region);
    }

    public List<Appartement> AppartementPiscine() {
        return appartementRepository.AppartementPiscine();
    }

    public List<Appartement> AppartementMontagne() {
        return appartementRepository.AppartementMontagne();
    }

    public List<AppartementDto> AppartementAvecId_Prix(LocalDate beginDate, LocalDate endDate) {
        return appartementRepository.AppartementAvecId_Prix(beginDate, endDate);
    }

    public List<AppartementDto> AppartementDto(LocalDate beginDate, LocalDate endDate, String region) {
        return appartementRepository.AppartementDto(beginDate, endDate, region);
    }

    public void deleteApp(Long id) {
        Appartement app = appartementRepository.findAppId(id);
        appartementRepository.delete(app);
    }
}

package com.ynov.demo.service;

import com.ynov.demo.domain.Appartement;
import com.ynov.demo.domain.Residence;
import com.ynov.demo.repository.AppartementRepository;
import com.ynov.demo.repository.ResidenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ResidenceService {
    private final ResidenceRepository residenceRepository;
    private AppartementRepository appartementRepository;

    public ResidenceService(ResidenceRepository residenceRepository, AppartementRepository appartementRepository) {
        this.residenceRepository = residenceRepository;
        this.appartementRepository = appartementRepository;
    }

    public List<Residence> getResidence() {
        List<Residence> residences = residenceRepository.getAllResidence();
        return residences;
    }

    public Residence createResidence(String nom, String type_residence, String pays, String region, String adresse, String gps, String type_lieu) {
        Residence residence = new Residence();
        residence.setNom(nom);
        residence.setType_residence(type_residence);
        residence.setPays(pays);
        residence.setRegion(region);
        residence.setAdresse(adresse);
        residence.setGps(gps);
        residence.setType_lieu(type_lieu);
        residenceRepository.save(residence);
        return residence;
    }

    @Transactional
    public Residence updateResidence(Long id, String nom, String type_residence, String pays,
                                   String region, String adresse, String gps, String type_lieu) {
        Residence residence = residenceRepository.findResidenceById(id);
        residence.setNom(nom);
        residence.setType_residence(type_residence);
        residence.setPays(pays);
        residence.setRegion(region);
        residence.setAdresse(adresse);
        residence.setGps(gps);
        residence.setType_lieu(type_lieu);
        residenceRepository.save(residence);
        System.out.println(residence.getAppartements());
        return residence;
    }

    public List<Residence> ResidencePays(String pays) {
        return residenceRepository.ResidencePays(pays);
    }

    public void deleteResidence(Long id) {
        Residence residence = residenceRepository.findResidenceById(id);
        List<Appartement> apps = appartementRepository.findAppartementsWithResidenceId(id);
        for (Appartement temp : apps) {
            appartementRepository.delete(temp);
        }
        residenceRepository.delete(residence);
    }

}

package com.ynov.demo.service;

import com.ynov.demo.domain.ServiceResidence;
import com.ynov.demo.domain.Residence;
import com.ynov.demo.repository.ServiceResidenceRepository;
import com.ynov.demo.repository.ResidenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ServiceResidenceService {
    private final ServiceResidenceRepository serviceResidenceRepository;
    private final ResidenceRepository residenceRepository;

    public ServiceResidenceService(ServiceResidenceRepository serviceResidenceRepository, ResidenceRepository residenceRepository) {
        this.serviceResidenceRepository = serviceResidenceRepository;
        this.residenceRepository = residenceRepository;
    }

    @Transactional
    public List<ServiceResidence> getServices() {
        return serviceResidenceRepository.getAllServices();
    }

    @Transactional
    public ServiceResidence createService(String detail_service, Long residence_id) {
        Residence residence = residenceRepository.findResidenceById(residence_id);
        ServiceResidence serviceResidence = new ServiceResidence();
        serviceResidence.setDetailService(detail_service);
        serviceResidenceRepository.save(serviceResidence);
        residence.getServiceResidences().add(serviceResidence);
        return serviceResidence;
    }

    @Transactional
    public ServiceResidence updateService(Long id, String detail_service, Long residence_id) {
        Residence oldResidence = residenceRepository.findResidenceById(residence_id);
        ServiceResidence serviceResidence = serviceResidenceRepository.findServiceById(id);
        oldResidence.getAppartements().remove(serviceResidence);
        Residence residence = residenceRepository.findResidenceById(residence_id);
        serviceResidence.setDetailService(detail_service);
        serviceResidenceRepository.save(serviceResidence);
        residence.getServiceResidences().add(serviceResidence);
        return serviceResidence;
    }

    public void deleteServiceResidence(Long id) {
        ServiceResidence serviceResidence = serviceResidenceRepository.findServiceById(id);
        serviceResidenceRepository.delete(serviceResidence);
    }
}

package com.ynov.demo.domain;

public class AppartementDto {
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType_lieu() {
        return type_lieu;
    }

    public void setType_lieu(String type_lieu) {
        this.type_lieu = type_lieu;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    private Long id;
    private String type_lieu;
    private int prix;
    private String nom_residence;

    public AppartementDto(Long id, String type_lieu, int prix, String nom_residence) {
        this.id = id;
        this.type_lieu = type_lieu;
        this.prix = prix;
        this.nom_residence = nom_residence;
    }

    public String getNom_residence() {
        return nom_residence;
    }

    public void setNom_residence(String nom_residence) {
        this.nom_residence = nom_residence;
    }

    public AppartementDto(Long id, String type_lieu, int prix) {
        this.id = id;
        this.type_lieu = type_lieu;
        this.prix = prix;
    }
}

package com.ynov.demo.domain;

import javax.persistence.*;

@Entity
public class ServiceResidence {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String detailService;

    public Long getId() {
        return id;
    }

    public String getDetailService() {
        return detailService;
    }

    public void setDetailService(String detailService) {
        this.detailService = detailService;
    }

}

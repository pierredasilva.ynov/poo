package com.ynov.demo.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"name"})})
public class Appartement {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @OneToMany
    @JoinColumn(name="APPARTEMENT_ID")
    private Set<ReservationDate> reservationDates;

    private String name;
    private int nombre_couchage;
    private int surface;
    private boolean equipement_bebe;
    private boolean climatisation;
    private int prix;

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public boolean getEquipement_bebe() {return equipement_bebe;}

    public boolean getClimatisation() {return climatisation;}

    public void setEquipement_bebe(boolean equipement_bebe) {this.equipement_bebe = equipement_bebe;}

    public void setClimatisation(boolean climatisation) {this.climatisation = climatisation;}

    public Long getId() {return id;}

    public void setNom(String nom) {this.name = nom;}

    public String getNom() {return name;}

    public int getNombre_couchage() {return nombre_couchage;}

    public void setNombre_couchage(int nombre_couchage) {this.nombre_couchage = nombre_couchage;}

    public int getSurface() {return surface;}

    public void setSurface(int surface) {this.surface = surface;}

    public Set<ReservationDate> getReservationDates() {
        return reservationDates;
    }

    public void setReservationDates(Set<ReservationDate> reservationDates) {
        this.reservationDates = reservationDates;
    }

}

package com.ynov.demo.domain;

import org.hibernate.annotations.DynamicUpdate;
import javax.persistence.*;
import java.util.Set;

@Entity
@DynamicUpdate
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"nom"})})
public class Residence {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String nom;
    private String type_residence;
    private String pays;
    private String region;
    private String adresse;
    private String gps;
    private String type_lieu;

    @OneToMany
    @JoinColumn(name="RESIDENCE_ID")
    private Set<Appartement> appartements;

    @OneToMany
    @JoinColumn(name="RESIDENCE_ID")
    private Set<ServiceResidence> serviceResidences;

    public Long getId() {
        return id;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getType_lieu() {
        return type_lieu;
    }

    public void setType_lieu(String type_lieu) {
        this.type_lieu = type_lieu;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType_residence() {
        return type_residence;
    }

    public void setType_residence(String type_residence) {
        this.type_residence = type_residence;
    }

    public void setAppartements(Set<Appartement> app) {
        this.appartements = app;
    }

    public Set<Appartement> getAppartements() {
        return appartements;
    }

    public Set<ServiceResidence> getServiceResidences() {
        return serviceResidences;
    }

    public void setServiceResidences(Set<ServiceResidence> serviceResidences) {
        this.serviceResidences = serviceResidences;
    }

}
